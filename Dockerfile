FROM renovate/pip
RUN pip install ansible-lint
ENTRYPOINT ["/home/ubuntu/.local/bin/ansible-lint"]
CMD ["--help"]
